import os, shutil
from jinja2 import Template
from pathlib import Path
from .templates.rust import basic
from importlib.resources import contents
import shutil

def copy_from_package_data(source_file_name, destination_folder):
    file_path = os.path.join(os.path.split(__file__)[0], source_file_name)
    if os.path.exists(file_path):
        shutil.copy2(file_path, destination_folder)
        print(source_file_name + " copied !")
    else:
        print("file doesn't exists")

def new(
    package_name,
    author,
    author_email,
    project_folder,
):
    project_folder = project_folder + ('' if project_folder[-1] == '/' else '/')
    Path(project_folder).mkdir(parents=True, exist_ok=True)

    # Create Cargo.toml
    cargo_toml_output = basic.get_cargo_toml(
        package_name=package_name,
        author=author,
        author_email=author_email,
    )

    print("create Cargo.toml")
    with open(project_folder + 'Cargo.toml', 'w') as f:
        f.write(cargo_toml_output)

    # Create src folder
    Path(project_folder + "src").mkdir(parents=True, exist_ok=True)

    # Create src folder
    Path(project_folder + "src/modules/basic/handlers").mkdir(parents=True, exist_ok=True)
    Path(project_folder + "src/modules/basic/models").mkdir(parents=True, exist_ok=True)

    # Create main.rs
    main_rs_data = copy_from_package_data("data/rust/src/main.rs", project_folder + "src/main.rs")
    main_rs_data = copy_from_package_data("data/rust/src/routes.rs", project_folder + "src/routes.rs")
    main_rs_data = copy_from_package_data("data/rust/Dockerfile", project_folder + "Dockerfile")
    main_rs_data = copy_from_package_data("data/rust/.gitignore", project_folder + ".gitignore")
