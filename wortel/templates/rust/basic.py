from jinja2 import Template

def get_cargo_toml(
    package_name,
    author,
    author_email,
):
    template = Template("""[package]
name = "{{package_name}}"
version = "0.1.0"
authors = ["{{author}} <{{author_email}}>"]
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
tokio = { version = "1", features = ["full"]  }
tokio-postgres = { version = "0.7", features = ["with-uuid-0_8"]  }
deadpool-postgres = { version = "0.7" }
warp = "0.3"
serde = {version = "1.0", features = ["derive"] }
serde_json = "1.0"
jsonwebtoken = "7"
chrono = "0.4"
thiserror = "1.0"
dotenv = "0.15.0"
bcrypt = "0.8"
bytes = { version = "1", features = ["serde"]   }
futures = { version = "0.3", default-features = false   }
uuid = "0.8.2"
rand = "0.8.3"
""")

    output = template.render(
        package_name=package_name,
        author=author,
        author_email=author_email,
    )

    return output

def get_main_rs():
    template = Template("""mod routes;
mod modules;

async fn create_assets_folder() -> std::io::Result<()> {
    println!("create dir");
    std::fs::create_dir_all("/assets/pendataan")?;

    let paths = std::fs::read_dir("/").unwrap();
    for path in paths {
        println!("Name: {}", path.unwrap().path().display())
    }
    Ok(())
}

#[tokio::main]
async fn main() {

    match create_assets_folder().await {
        Ok(_) => {println!("berhasil buat dir baru")},
        Err(e) => {println!("gagal buat dir baru karena {:?}", e)},
    };

    let routes = routes::apply();

    println!("Server started...");
    warp::serve(routes)
        .run(([0, 0, 0, 0], 3030))
        .await;
}
    """)

    output = template.render()

    return output
