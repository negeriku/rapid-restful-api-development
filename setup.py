import setuptools

package_name = "wortel"

setuptools.setup(
    name="wortel",
    version="0.0.1",
    author="Negeriku.id",
    author_email="insan@mecha.id",
    description="Rapid RESTful API development in Rust",
    packages=[package_name],
    classifiers=[
        "Programming Language :: Python :: 3",
    ],
    python_requires=">3.6",
    install_requires=[
        'jinja2',
    ],
)
